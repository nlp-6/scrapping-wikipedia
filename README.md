Intrucciones
Se creo un archivos en Python de nombre TareaFinal2.jpynb el cual permite crear 3 archivos.

Luego unimos el contenido de los 3 archivos para poder realizar tokenizacion en ellos.

para ellos probamos cargar hasta 1mm de records en cada archivo pero haciendo una prueba de media
hora nos demoramos 35 minutos entonces mejor para nuestra meustra lo reducimos a 4k registros
y con ello pudimos realizar nuestra tokenizacion de mejor manera.

Al final utilizamos una constante en la cual colocamos nosotros cuantos tokes queremos evaluar
y mediante esta variable vamos viendo nosotros cada token unico.

al final obtenemos unas graficas en las cuales podemos observar el resultado de nuestra tokenizacion.

Forma de la grafica
Cantidad de tokes, tokens unicos y relacion entre tokes y tokens unicos.
