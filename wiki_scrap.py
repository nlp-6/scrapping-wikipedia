###############################
#Grupo 6
#Honatan Alexander Palma Che
#Edgar Geovani Garcia Lopez
#Jorge Ramiro Ibarra Monroy
############################
#Se instala lxml si no esta disponible
#!pip install lxml
from lxml import html
import json
import requests
from time import sleep

#URL para realizacion de scrapping
url_wiki = 'https://en.wikipedia.org/wiki/Wikipedia:Featured_articles'
#Metodo requests para recuperacion de datos
response = requests.get(url_wiki)
#Conversion de texto plano a formato HTML
parser = html.fromstring(response.text)
#Obtencion del listado de articulos
articulos = parser.xpath("(//div[@class='hlist'])[2]/p/span/a/@href")
#print (articulos)
#Declaracion de variables
list = []
list2 = []
list3 = []
count = 0
#Recorrido de articulos
for ar in articulos:
    artdict = {}
    link = "https://en.wikipedia.org"+ar
    response2 = requests.get(link)
    parser = html.fromstring(response2.text)
    titulo = parser.xpath("//h1[@id='firstHeading']/text()")
    parrafo = parser.xpath("//div[@class='mw-parser-output']/p[2]/text()")
    #Depura texto recuperado de caracteres especiales
    lista_temp = titulo + ' '.join(parrafo).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("\u0101"," ").replace("Ā"," ").replace(":"," ").replace(";"," ").replace("."," ").replace(","," ").replace("("," ").replace(")"," ").replace("["," ").replace("]"," ").replace("{"," ").replace("}"," ").split(" ")
    while '' in lista_temp:
        lista_temp.remove('')
    list.extend(lista_temp)
    cont = len(list)
    texto = ' '.join(lista_temp)+'\n'
    #Apertura de archivos para escritura de texto
    f = open("C:/MAESTRIA/Aquisition/wiki/datawikiA.csv","a",encoding='utf-8')
    f.write(texto)
    f.close()
    if(count % 5 == 0):
        texto = ' '.join(lista_temp)+'\n'
        f = open("C:/MAESTRIA/Aquisition/wiki/datawikiB.csv","a",encoding='utf-8')
        f.write(texto)
        f.close()
    if(count % 10 == 0):
        texto = ' '.join(lista_temp)+'\n'
        f = open("C:/MAESTRIA/Aquisition/wiki/datawikiC.csv","a",encoding='utf-8')
        f.write(texto)
        f.close()
    #Tamano de la muestra
    if count > 4000 : 
        break
    count += 1
